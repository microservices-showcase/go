#!/bin/bash

set -e
source .ci/common.sh

# the tag should have format VERSION-SERVICE_NAME
# we'll extract SERVICE_NAME here and build docker image

service_name=$(echo ${CI_COMMIT_REF_SLUG} | sed -E 's~.+-(.+)$~\1~')
if [[ -z "$service_name" ]]; then
    error "invalid service name, this could be caused by invalid format env \$CI_COMMIT_REF_SLUG"
    exit 1
fi

echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY

package="${ROOT_PACKAGE}/services/${service_name}/cmd"
image="${CI_REGISTRY_IMAGE}/${service_name}:${CI_COMMIT_REF_SLUG}"

build_arg="--build-arg VERSION=${CI_COMMIT_REF_SLUG} --build-arg BUILD_PKG=${package}"

if [[ -d "services/${service_name}/config" ]]; then
    build_arg="${build_arg} --build-arg COPY_CONFIG=services/${service_name}/config"
fi

info "🚀 building ${image} ${build_arg}"
docker build -t ${image} ${build_arg} .
docker push ${image}
