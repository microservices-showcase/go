#!/bin/bash

# Utilities
function info() { printf "\033[0;92m%b\033[0m\n" "${*}"; }
function warn() { printf "\033[0;93m%b\033[0m\n" "${*}"; }
function error() { printf "\033[0;91m%b\033[0m\n" "${*}"; }
function finish() { printf "\n\n"; exit "$1"; }
