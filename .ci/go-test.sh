#!/bin/bash

source .ci/common.sh

# Fetch remote ref
info "\n=== FETCHING REMOTE REF ==="
git --no-pager fetch --force origin "$CI_COMMIT_REF_NAME":"$CI_COMMIT_REF_NAME"
git --no-pager fetch --force origin master:master
if [ -n "$CI_MERGE_REQUEST_IID" ]; then
  git --no-pager fetch --force origin "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME":"$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
fi

# Print commit information
info "\n=== COMMIT INFORMATION ==="
git --no-pager show --quiet "$CI_COMMIT_SHA"

# Select diff base
info "\n=== SELECTING DIFF BASE ==="
if [ -n "$CI_MERGE_REQUEST_IID" ]; then
  diff_base=$(git merge-base "$CI_COMMIT_SHA" "origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME")
  diff_base=${diff_base:-"origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"}
elif [ "$CI_COMMIT_REF_NAME" = "master" ]; then
  if [ "$(git cat-file -e "$CI_COMMIT_BEFORE_SHA" && git --no-pager branch --contains "$CI_COMMIT_BEFORE_SHA" | grep -E "(^|\s)$CI_COMMIT_REF_NAME$")" ]; then
    diff_base=$CI_COMMIT_BEFORE_SHA
  else
    warn "💥💥💥 Force push detected 💥💥💥"
    warn "Can't find diff base $CI_COMMIT_BEFORE_SHA in $CI_COMMIT_REF_NAME. Skipping"
    finish 1
  fi
else
  diff_base=$(git merge-base "$CI_COMMIT_SHA" origin/master)
  diff_base=${diff_base:-origin/master}
fi
echo "Diff base: $diff_base"

changed_files=$(git --no-pager diff --name-only "$diff_base".."$CI_COMMIT_SHA" | grep -E '^(services|lib)/')

if [ -z "$changed_files" ]; then
  info "No changes detected"
  finish 0
fi

# take packages needs to be tested
packages=$(printf "%s\n" $changed_files | sed -E 's~^([^/]+/[^/]+)/.+~\1~' | uniq)

info "\n=== START TESTING ==="
printf "📦 %s\n" $packages
info "\n====================="

echo "mode: atomic" > coverage.txt
for p in $packages; do
    go test -coverprofile=profile.out -covermode=atomic ./$p/...
    if [ -f profile.out ]; then
        tail -n +2 profile.out >> coverage.txt
        rm profile.out
    fi
done
go tool cover -func coverage.txt

# store changed services to build
printf "%s\n" $changed_files | grep -E '^services/' | sed -E 's~^services/([^/]+)/.+~\1~' | uniq | tee changed_services.txt
