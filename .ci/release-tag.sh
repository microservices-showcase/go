#!/bin/bash

version_base=$(date +%Y%m%d)-${CI_PIPELINE_IID}

services=$(cat changed_services.txt)
for service in $services; do
    tag="${version_base}-${service}"

    url="${GITLAB_API_URL}/api/v4/projects/${CI_PROJECT_ID}/repository/tags?tag_name=${tag}&ref=${CI_COMMIT_SHA}&private_token=${GITLAB_TOKEN}"
    echo "creating tag ${tag}: ${url}"
    curl -X POST $url

done
