FROM golang:1.14

ARG VERSION=0.0.0
ARG BUILD_PKG
ARG COPY_CONFIG

ENV CGO_ENABLED=0 GO111MODULE=on

WORKDIR /src
ADD go.mod /src
ADD go.sum /src

# cache go modules first
RUN go mod download

ADD . /src

RUN echo "Build version: ${VERSION}"
RUN go build -ldflags="-s -w -X main.version=${VERSION}" -o /app ${BUILD_PKG}
RUN mkdir -p ./config && (cp -r ${COPY_CONFIG} ./ >/dev/null 2>&1 || true)

FROM alpine:3.11
COPY --from=0 /app /usr/bin/app
COPY --from=0 /src/config /app/config
WORKDIR /app

CMD ["/usr/bin/app"]
