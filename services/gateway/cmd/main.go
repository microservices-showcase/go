package main

import (
    "fmt"
    "github.com/caarlos0/env/v6"
    "github.com/gin-contrib/cors"
    "github.com/gin-gonic/gin"
    "google.golang.org/grpc"
    "jx/central/go/lib/core"
    "jx/central/go/services/authx/pb"
    "jx/central/go/services/gateway"
    "jx/central/go/services/gateway/service/plugins"
    "jx/central/go/services/gateway/service/registry"
    "os"
    "strings"

    "github.com/sirupsen/logrus"
)

var log *logrus.Entry

var (
    serviceName = "gateway"
    version     = "0.0.0"
)

func init() {
    log = core.MakeLoggerByEnv("service:" + serviceName)
}

func main() {
    log.Info("load configuration ...")
    config := gateway.AppConfig{}
    if err := env.Parse(&config); err != nil {
        log.WithError(err).Fatalln("failed to parse configuration")
    }

    // register routes
    gin.SetMode(gin.ReleaseMode)
    r := gin.New()
    r.Use(
        cors.Default(),
        core.GlobalTracingHandler(serviceName),
        core.GlobalRequestLog(serviceName, log),
    )

    r.GET("/", core.HealthHandler(serviceName, version))
    r.NoRoute(core.NotFoundHandler())

    // authx grpc connection
    var authxClient pb.AuthxClient
    if config.UseAuthxGRPC && len(config.AuthxAddress) > 0 {
        authxConn, err := grpc.Dial(config.AuthxAddress, grpc.WithInsecure())
        if err != nil {
            log.WithError(err).Error("failed to open connection to Authx(grpc)")
        } else {
            defer func() {
                _ = authxConn.Close()
            }()
            authxClient = pb.NewAuthxClient(authxConn)
        }
    }
    authPlugin := plugins.NewAuthPlugin(config.AuthServiceURL, authxClient)
    serviceRegistry := registry.NewRegistry(r, authPlugin.GetAuthorizer(), log)

    // load services from files
    for _, s := range strings.Split(config.OnboardServices, ",") {
        s = strings.TrimSpace(s)
        path := fmt.Sprintf("./config/services/%s.yml", s)
        if _, err := os.Stat(path); err != nil {
            log.WithField("file_path", path).Info("File is not existed")
            continue
        }
        apiService, err := registry.LoadServiceFromFile(path)
        if err != nil {
            log.WithError(err).Error("failed to load service")
            continue
        }
        err = serviceRegistry.RegisterService(apiService)
        if err != nil {
            log.WithError(err).Error("failed to register service")
        } else {
            log.WithField("serviceID", apiService.ID).Info("registered service")
        }
    }

    v := make(chan struct{})
    go func() {
        if err := r.Run(config.ListenAddr); err != nil {
            log.WithError(err).Error("failed to start service")
            close(v)
        }
    }()
    log.WithField("listen_addr", config.ListenAddr).Info("start listening")
    <-v
}
