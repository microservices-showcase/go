package gateway

// AppConfig presents app config
type AppConfig struct {
    ListenAddr      string `env:"APP_LISTEN" envDefault:"0.0.0.0:8080"`
    AuthServiceURL  string `env:"AUTH_SERVICE" envDefault:"http://authx"`
    UseAuthxGRPC    bool   `env:"USE_AUTHX_GRPC" envDefault:"false"`
    AuthxAddress    string `env:"AUTHX_ADDRESS" envDefault:"authx:50551"`
    OnboardServices string `env:"ONBOARD_SERVICES" envDefault:"echo,authx"`
}

// trigger ci 2020-06-02 1591096668
