package registry

import (
    "fmt"
    "github.com/gin-gonic/gin"
    "github.com/sirupsen/logrus"
    "io"
    "net/http"
    "time"
)

// Registry provides storage for services and routes
type Registry interface {
    // RegisterService register all service's routes into gin Engine
    RegisterService(s *Service) error
}

// HttpClient ...
type HttpClient interface {
    Do(req *http.Request) (*http.Response, error)
}

type registryImpl struct {
    engine         *gin.Engine
    authMiddleware gin.HandlerFunc
    client         HttpClient
    log            *logrus.Entry
}

func (reg *registryImpl) terminate(c *gin.Context, code int, reason string) {
    c.JSON(code, gin.H{"error": reason})
    c.Abort()
}

func (reg *registryImpl) request(c *gin.Context, route *Route) {
    // build url to upstream
    url := route.S.Host + route.Path
    if c.Request.URL.RawQuery != "" {
        url += "?" + c.Request.URL.RawQuery
    }

    req, err := http.NewRequestWithContext(c, route.Method, url, c.Request.Body)
    if err != nil {
        reg.terminate(c, http.StatusInternalServerError, "failed to init request: "+err.Error())
        return
    }

    // forward authorized data
    if c.GetString("x-user-id") != "" {
        req.Header.Set("x-user-id", c.GetString("x-user-id"))
        req.Header.Set("x-user-role", c.GetString("x-user-role"))
    }
    req.Header.Add("x-trace", "gateway")
    req.Header.Add("user-agent", "gateway/v0.0.0")

    start := time.Now()
    rsp, err := reg.client.Do(req)
    latency := time.Now().Sub(start)
    c.Header("content-type", "application/json")
    c.Header("x-upstream-latency-in-ms", fmt.Sprintf("%d", latency.Milliseconds()))

    if err != nil {
        reg.terminate(c, http.StatusInternalServerError, "failed to request to upstream: "+err.Error())
        return
    }

    // response
    c.Status(rsp.StatusCode)
    if rsp.Header.Get("x-trace") != "" {
        c.Header("x-trace", rsp.Header.Get("x-trace"))
    } else {
        // there is no x-trace from upstream, show default
        c.Header("x-trace", "gateway;" + route.S.ID)
    }
    if rsp.Body != nil {
        n, err := io.Copy(c.Writer, rsp.Body)
        if err != nil {
            reg.log.WithError(err).Error("error while copying upstream response")
        } else {
            reg.log.WithField("copied_bytes", n).Debug("copied upstream response bytes")
        }
        _ = rsp.Body.Close()
    }
}

// RegisterService register all service's routes into gin Engine
func (reg *registryImpl) RegisterService(s *Service) error {
    if s.ID == "" {
        panic("empty service ID")
    }

    for _, route := range s.Routes {
        if route.S == nil {
            route.S = s
        }
        if err := reg.registerRoute(route); err != nil {
            return err
        }
    }
    return nil
}

func (reg *registryImpl) registerRoute(route *Route) error {
    handlers := gin.HandlersChain{}
    if route.ReqAuth {
        handlers = append(handlers, reg.authMiddleware)
    }
    handlers = append(handlers, reg.makeRouteProxyHandler(route))

    reg.engine.Handle(route.Method, route.GetFinalPath(), handlers...)

    return nil
}

func (reg *registryImpl) makeRouteProxyHandler(r *Route) gin.HandlerFunc {
    return func(c *gin.Context) {
        reg.request(c, r)
    }
}

// NewRegistry ...
func NewRegistry(engine *gin.Engine, authMiddleware gin.HandlerFunc, log *logrus.Entry) Registry {
    return &registryImpl{
        engine:         engine,
        authMiddleware: authMiddleware,
        log:            log,
        client:         &http.Client{Timeout: time.Second * 10},
    }
}
