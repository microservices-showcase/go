package registry

import (
    "github.com/gin-gonic/gin"
    "github.com/stretchr/testify/mock"
    "github.com/stretchr/testify/suite"
    "jx/central/go/lib/core"
    "jx/central/go/services/gateway/service/registry/mocks"
    "io/ioutil"
    "net/http"
    "net/http/httptest"
    "strings"
    "testing"
)

type registryTestSuite struct {
    suite.Suite

    reg *registryImpl
}

func (ts *registryTestSuite) SetupTest() {
    gin.SetMode(gin.TestMode)
    log := core.MakeLoggerByEnv("env:test")

    ts.reg = &registryImpl{
        engine: gin.New(),
        authMiddleware: func(c *gin.Context) {
            c.Set("x-user-id", "1")
        },
        client: nil,
        log:    log,
    }
}

func TestRegistrySuite(t *testing.T) {
    suite.Run(t, new(registryTestSuite))
}

func (ts *registryTestSuite) onboardEcho() {
    s := &Service{
        ID:   "echo",
        Host: "http://echo.stg.service",
        Routes: []*Route{
            &Route{
                Path:     "/status",
                Method:   http.MethodGet,
                StripSID: false,
                ReqAuth:  false,
            },

            &Route{
                Path:     "/public-echo",
                Method:   http.MethodGet,
                StripSID: true,
                ReqAuth:  false,
            },
        },
    }

    err := ts.reg.RegisterService(s)
    ts.Assert().NoError(err)
}

func (ts *registryTestSuite) TestRegisterService() {
    ts.onboardEcho()

    ts.Assert().Equal("/echo/status", ts.reg.engine.Routes()[0].Path)
    ts.Assert().Equal(http.MethodGet, ts.reg.engine.Routes()[0].Method)

    ts.Assert().Equal("/public-echo", ts.reg.engine.Routes()[1].Path)
}

func (ts *registryTestSuite) TestRequest() {
    ts.onboardEcho()
    w := httptest.NewRecorder()
    req, _ := http.NewRequest("GET", "/echo/status", nil)

    client := &mocks.HttpClient{}
    rsp := &http.Response{
        StatusCode: 200,
        Body: ioutil.NopCloser(strings.NewReader("test")),
    }
    client.On("Do", mock.MatchedBy(func (req *http.Request) bool {
        return req.URL.String() == "http://echo.stg.service/status"
    })).Return(rsp, nil)

    ts.reg.client = client

    ts.reg.engine.ServeHTTP(w, req)
    ts.Assert().Equal(200, w.Code)
    ts.Assert().Equal("test", w.Body.String())
}
