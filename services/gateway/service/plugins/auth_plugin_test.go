package plugins

import (
    "github.com/gin-gonic/gin"
    "github.com/stretchr/testify/mock"
    "github.com/stretchr/testify/suite"
    "jx/central/go/services/authx/pb"
    "jx/central/go/services/authx/pb/mocks"
    "net/http"
    "net/http/httptest"
    "testing"
)

type authPluginTestSuite struct {
    suite.Suite
}

func TestAuthPlugin(t *testing.T) {
    suite.Run(t, new(authPluginTestSuite))
}

func (ts *authPluginTestSuite) TestInitialize() {
    plug := NewAuthPlugin("http://authx/", nil)
    ts.Assert().NotNil(plug)
    p, ok := plug.(*authPluginImpl)
    ts.Assert().True(ok)
    ts.Assert().Equal("http://authx", p.authxServer)
}

func (ts *authPluginTestSuite) TestInitializeError() {
    ts.Assert().Panics(func() {
        NewAuthPlugin("", nil)
    }, "should panic when initializing with empty http server or gRPC client either")
}

func (ts *authPluginTestSuite) TestAuthValidateGRPC() {
    authxClient := &mocks.AuthxClient{}
    authxClient.On(
        "Validate",
        mock.Anything,
        mock.AnythingOfType("*pb.ValidateRequest")).
        Return(&pb.ValidateResponse{
            Code:   200,
            UserID: 1,
            Role:   "admin",
            Error:  "",
        }, nil)
    plug := NewAuthPlugin("", authxClient)
    w := httptest.NewRecorder()
    gin.SetMode(gin.TestMode)
    c, _ := gin.CreateTestContext(w)
    c.Request, _ = http.NewRequest("GET", "/", nil)
    c.Request.Header.Set("Authorization", "Bearer test-token")
    plug.GetAuthorizer()(c)

    assertCtxValue := func(key, value string) {
        got, ok := c.Get(key)
        ts.Assert().True(ok)
        ts.Assert().Equal(value, got)
    }

    assertCtxValue("x-user-id", "1")
    assertCtxValue("x-user-role", "admin")
}

