package plugins

import (
    "bytes"
    "context"
    "encoding/json"
    "errors"
    "fmt"
    "github.com/gin-gonic/gin"
    "jx/central/go/services/authx/pb"
    "io/ioutil"
    "net/http"
    "strings"
    "time"
)

// AuthPlugin plugin handles authorization
// It has a method to get authorizer which will contact to authx service
// to authorize the request and set respective headers to context
type AuthPlugin interface {
    GetAuthorizer() gin.HandlerFunc
}

type validateStrategy func(reqData *pb.ValidateRequest) (*pb.ValidateResponse, error)

type authPluginImpl struct {
    authxServer string
    httpClient  *http.Client
    authxClient pb.AuthxClient
    validate    validateStrategy
}

const (
    authPluginUserAgent = "gateway-auth/1.0"
)

// GetAuthorizer get the middleware
func (a *authPluginImpl) GetAuthorizer() gin.HandlerFunc {
    return a.authorize
}

func (a *authPluginImpl) terminate(c *gin.Context, code int, reason string) {
    c.JSON(code, gin.H{"error": reason})
    c.Abort()
}

func (a *authPluginImpl) authorize(c *gin.Context) {
    // extract token
    token := ""
    authHeader := c.GetHeader("authorization")
    if authHeader != "" {
        token = strings.Replace(authHeader, "Bearer ", "", 1)
    }

    if token == "" {
        token = c.Query("token")
    }
    if token == "" {
        a.terminate(c, http.StatusUnauthorized, "invalid authorization header")
        return
    }

    reqData := &pb.ValidateRequest{
        Token: token,
        Scope: c.Request.URL.Path,
        Op:    c.Request.Method,
    }

    rspData, err := a.validate(reqData)
    if err != nil {
        a.terminate(c, http.StatusInternalServerError, err.Error())
        return
    }
    if !a.isAuthorizedResponse(rspData) {
        a.terminate(c, int(rspData.Code), rspData.Error)
        return
    }

    // authorized, set context values and go ahead
    c.Set("x-user-id", fmt.Sprintf("%d", rspData.UserID))
    c.Set("x-user-role", rspData.Role)
    c.Next()
}

func (a *authPluginImpl) isAuthorizedResponse(rspData *pb.ValidateResponse) bool {
    return int(rspData.Code) == http.StatusOK
}

// doHttpStrategy validate to authx via http
// POST /validate
func (a *authPluginImpl) doHttpStrategy(reqData *pb.ValidateRequest) (*pb.ValidateResponse, error) {
    url := a.authxServer + "/validate"
    bPayload, _ := json.Marshal(reqData)
    req, _ := http.NewRequest("POST", url, bytes.NewReader(bPayload))
    req.Header.Set("user-agent", authPluginUserAgent)
    rsp, err := a.httpClient.Do(req)
    if err != nil {
        return nil, err
    }
    if rsp.StatusCode != http.StatusOK {
        msg := fmt.Sprintf("error while authorizing, #status: %d", rsp.StatusCode)
        return &pb.ValidateResponse{
            Code:   uint32(rsp.StatusCode),
            UserID: 0,
            Role:   "",
            Error:  msg,
        }, nil
    }
    rspData := &pb.ValidateResponse{}
    bPayload, err = ioutil.ReadAll(rsp.Body)
    _ = rsp.Body.Close()
    if err != nil {
        return nil, errors.New("error while reading response: " + err.Error())
    }
    err = json.Unmarshal(bPayload, &rspData)
    if err != nil {
        return nil, errors.New("error while decoding response: " + err.Error())
    }

    return rspData, nil
}

func (a *authPluginImpl) doGRPCStrategy(reqData *pb.ValidateRequest) (*pb.ValidateResponse, error) {
    ctx, cancelFn := context.WithTimeout(context.Background(), time.Second*10)
    defer cancelFn()
    rspData, err := a.authxClient.Validate(ctx, reqData)
    return rspData, err
}

// NewAuthPlugin make new plugin instance
func NewAuthPlugin(authxServer string, authxClient pb.AuthxClient) AuthPlugin {
    if authxServer == "" && authxClient == nil {
        panic(errors.New("could not initialize AuthPlugin: no authx http server or gRPC client supplied"))
    }

    // strip last "/" in authxServer
    if authxServer != "" {
        authxServer = strings.TrimRight(authxServer, "/")
    }

    plug := &authPluginImpl{
        authxServer: authxServer,
        httpClient:  &http.Client{Timeout: time.Second * 10},
        authxClient: authxClient,
    }

    if authxClient != nil {
        plug.validate = plug.doGRPCStrategy
    } else {
        plug.validate = plug.doHttpStrategy
    }

    return plug
}
