package main

import (
    "github.com/caarlos0/env/v6"
    "github.com/casbin/casbin/v2"
    gormadapter "github.com/casbin/gorm-adapter/v2"
    "github.com/gin-gonic/gin"
    "github.com/sirupsen/logrus"
    "google.golang.org/grpc"
    "jx/central/go/lib/core"
    "jx/central/go/services/authx"
    "jx/central/go/services/authx/model"
    "jx/central/go/services/authx/pb"
    "jx/central/go/services/authx/repository"
    "jx/central/go/services/authx/service"
    "jx/central/go/services/authx/service/token"
    "net"
)

var log *logrus.Entry

var (
	serviceName = "authx"
	version     = "0.0.0"
)

func init() {
	log = core.MakeLoggerByEnv("service:" + serviceName)
}

func main() {
	config := authx.AppConfig{}
	_ = env.Parse(&config)

	db, err := core.NewDBConnection(config.DBConfig)
	if err != nil {
		log.WithError(err).Error("failed to open DB connection")
		return
	}
	defer core.CloseDB(db)

	// casbin enforcer
	enforcerAdapter, err := gormadapter.NewAdapterByDB(db)
	if err != nil {
		log.WithError(err).Error("failed to init casbin DB adapter")
		return
	}
	enforcer, err := casbin.NewEnforcer("config/rbac_model.conf", enforcerAdapter)
	if err != nil {
		log.WithError(err).Error("failed to init casbin enforcer")
		return
	}
	if err = enforcer.LoadPolicy(); err != nil {
		log.WithError(err).Error("failed to load policy")
		return
	}

	userRepository := repository.NewUserRepository(db)
	toker := token.NewTokenGenerator([]byte(config.SignKey))
	authxService := service.NewAuthXService(userRepository, toker, log, enforcer)

	// register routes
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(core.GlobalTracingHandler(serviceName), core.GlobalRequestLog(serviceName,log))

	r.GET("/status", core.HealthHandler(serviceName, version))

	r.POST("/login", authxService.LoginHandler())
	r.POST("/validate", authxService.ValidateHandler())

	db.AutoMigrate(&model.User{})

	// TODO: remove in real life
	addTestUsers(userRepository)

	initPolicy(enforcer)

	v := make(chan struct{})
	go func() {
		if err := r.Run(config.ListenAddr); err != nil {
			log.WithError(err).Error("failed to start service")
			close(v)
		}
	}()
	log.WithField("listen_addr", config.ListenAddr).Info("start listening")

	// add gRPC listener
	go func() {
		server := grpc.NewServer()
		pb.RegisterAuthxServer(server, authxService)
		lis, err := net.Listen("tcp", config.GRPCListenAddr)
		if err != nil {
			log.WithError(err).Error("failed to listen gRPC")
			return
		}
		log.WithField("gRPC_listen", lis.Addr()).Info("[gRPC] start listening")
		err = server.Serve(lis)
		if err != nil {
			log.WithError(err).Error("failed to server gRPC")
		}
	}()
	<-v
}

// add some test users
func addTestUsers(userRepository repository.UserRepository) {
	_ = userRepository.Create(&model.User{
		Username: "admin",
		Password: "admin",
		Role:     "admin",
	})
	_ = userRepository.Create(&model.User{
		Username: "user1",
		Password: "user1",
		Role:     "user",
	})
	_ = userRepository.Create(&model.User{
		Username: "user2",
		Password: "user2",
		Role:     "user",
	})
}

func initPolicy(enforcer *casbin.Enforcer) {
	_, _ = enforcer.AddPolicy("user", "/articles/*", "GET")
	_, _ = enforcer.AddPolicy("admin", "/articles/*", "*")
	_ = enforcer.SavePolicy()
}
