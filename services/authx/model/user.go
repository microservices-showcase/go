package model

import (
	"golang.org/x/crypto/bcrypt"
	"time"
)

type User struct {
	ID        uint   `gorm:"primary_key" json:"id"`
	Username  string `sql:"index"`
	Password  string
	Role      string
	CreatedAt time.Time `sql:"index"`
}

// SetPassword ...
func (u *User) SetPassword(bPwd []byte) {
	hashed, _ := bcrypt.GenerateFromPassword(bPwd, bcrypt.DefaultCost)
	u.Password = string(hashed)
}

// IsValidPassword ...
func (u *User) IsValidPassword(pPwd []byte) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), pPwd)
	return err == nil
}
