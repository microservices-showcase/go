package authx

import "jx/central/go/lib/core"

// AppConfig presents app config
type AppConfig struct {
	core.DBConfig

	SignKey        string `env:"SIGN_KEY" envDefault:"secret"`
	ListenAddr     string `env:"APP_LISTEN" envDefault:"0.0.0.0:8080"`
	GRPCListenAddr string `env:"GRPC_LISTEN" envDefault:"0.0.0.0:55051"`
}

// test: trigger build this service 2020-06-02 1591096675
