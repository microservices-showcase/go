package service

import (
	"github.com/gin-gonic/gin"
	"jx/central/go/lib/core"
	"net/http"
)

type loginRequest struct {
	Username string `binding:"required"`
	Password string `binding:"required"`
}

type loginResponse struct {
	Code  int    `json:"code"`
	Error string `json:"error,omitempty"`
	Token string `json:"token,omitempty"`
}

func (a *authxServiceImpl) LoginHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		req := &loginRequest{}
		if err := c.ShouldBindJSON(req); err != nil {
			c.JSON(http.StatusBadRequest, err.Error())
			return
		}

		user, err := a.users.FindByUsername(req.Username)
		core.PanicOnError(err)

		rsp := loginResponse{
			Code:  http.StatusOK,
		}
		if user == nil || !user.IsValidPassword([]byte(req.Password)) {
			rsp.Code = http.StatusUnauthorized
			rsp.Error = "invalid credentials"
		} else {
			// return valid token
			rsp.Token, _ = a.toker.NewUserToken(user)
		}
		c.JSON(rsp.Code, rsp)
	}
}
