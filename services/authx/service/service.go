package service

import (
	"context"
	"jx/central/go/services/authx/pb"
	"jx/central/go/services/authx/service/token"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"jx/central/go/lib/core"
	"jx/central/go/services/authx/repository"
)

//go:generate mockery -name Enforcer

type AuthXService interface {
	LoginHandler() gin.HandlerFunc
	ValidateHandler() gin.HandlerFunc
	Validate(ctx context.Context, request *pb.ValidateRequest) (*pb.ValidateResponse, error)
}

// Enforcer an interface that fits with casbin.Enforcer struct
// to make testing easier
type Enforcer interface {
	Enforce(rvals ...interface{}) (bool, error)
}

// NewAuthXService ...
func NewAuthXService(
	users repository.UserRepository,
	tokener token.TokenGenerator,
	log *logrus.Entry,
	enforcer Enforcer) AuthXService {

	return &authxServiceImpl{
		users:    users,
		toker:    tokener,
		log:      log,
		enforcer: enforcer,
	}
}

type authxServiceImpl struct {
	users     repository.UserRepository
	toker     token.TokenGenerator
	log       *logrus.Entry
	enforcer  Enforcer
	validator core.Validator

	sync.Mutex
}

func (a *authxServiceImpl) getValidator() core.Validator {
	a.Lock()
	defer a.Unlock()
	if a.validator == nil {
		a.validator = core.NewValidator()
	}
	return a.validator
}
