package service

import (
    "context"
    "errors"
    "github.com/stretchr/testify/assert"
    "jx/central/go/services/authx/pb"
    svcMocks "jx/central/go/services/authx/service/mocks"
    "jx/central/go/services/authx/service/token"
    tokMocks "jx/central/go/services/authx/service/token/mocks"
    "net/http"
    "testing"
)

func TestAuthorizeRequestSuccess(t *testing.T) {
	req := &pb.ValidateRequest{
		Token: "token",
		Scope: "/authx",
		Op:    "POST",
	}

	toker := &tokMocks.TokenGenerator{}
	assumedClaims := &token.UserClaims{
		ID:   1,
		Role: "admin",
	}
	toker.On("ParseUserToken", req.Token).Once().Return(assumedClaims, nil)

	enforcer := &svcMocks.Enforcer{}
	enforcer.On("Enforce", "admin", req.Scope, req.Op).Once().Return(true, nil)
	authx := NewAuthXService(nil, toker, nil, enforcer)
	rsp := authx.(*authxServiceImpl).authorizeRequest(req)
	assert.Equal(t, uint32(http.StatusOK), rsp.GetCode())
	assert.Equal(t, uint32(assumedClaims.ID), rsp.GetUserID())
}

func TestAuthorizeRequestWithEnforceFalse(t *testing.T) {
	req := &pb.ValidateRequest{
		Token: "token",
		Scope: "/authx",
		Op:    "POST",
	}

	toker := &tokMocks.TokenGenerator{}
	assumedClaims := &token.UserClaims{
		ID:   1,
		Role: "user",
	}
	toker.On("ParseUserToken", req.Token).Once().Return(assumedClaims, nil)

	enforcer := &svcMocks.Enforcer{}
	enforcer.On("Enforce", "user", req.Scope, req.Op).Once().Return(false, nil)
	authx := NewAuthXService(nil, toker, nil, enforcer)
	rsp := authx.(*authxServiceImpl).authorizeRequest(req)
	assert.Equal(t, uint32(http.StatusForbidden), rsp.GetCode())
	assert.Equal(t, "Forbidden", rsp.Error)
}

func TestAuthorizeRequestWithEnforceError(t *testing.T) {
	req := &pb.ValidateRequest{
		Token: "token",
		Scope: "/authx",
		Op:    "POST",
	}

	toker := &tokMocks.TokenGenerator{}
	assumedClaims := &token.UserClaims{
		ID:   1,
		Role: "user",
	}
	toker.On("ParseUserToken", req.Token).Once().Return(assumedClaims, nil)

	enforcer := &svcMocks.Enforcer{}
	enforcer.On("Enforce", "user", req.Scope, req.Op).Once().Return(false, errors.New("enforce error"))
	authx := NewAuthXService(nil, toker, nil, enforcer)

	rsp, err := authx.(*authxServiceImpl).Validate(context.Background(), req)
	assert.NoError(t, err)
	assert.Equal(t, uint32(http.StatusForbidden), rsp.GetCode())
	assert.Equal(t, "enforce error", rsp.Error)
}

func TestValidateBadData(t *testing.T) {
	req := &pb.ValidateRequest{
		Token: "",
		Scope: "/authx",
		Op:    "POST",
	}
	authx := &authxServiceImpl{}
	_, err := authx.Validate(context.Background(), req)
	assert.Error(t, err)
}
