package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"jx/central/go/lib/core"
	"jx/central/go/services/authx/model"
	"jx/central/go/services/authx/pb"
	"jx/central/go/services/authx/repository"
	"jx/central/go/services/authx/service/mocks"
	"jx/central/go/services/authx/service/token"
	"net/http/httptest"
	"os"
	"testing"
)

type authxServiceTestSuite struct {
	suite.Suite

	db       *gorm.DB
	service  AuthXService
	user     *model.User
	users    repository.UserRepository
	toker    token.TokenGenerator
	enforcer *mocks.Enforcer
}

func (ts *authxServiceTestSuite) SetupTest() {
	var err error
	log := logrus.New()
	ts.db, err = core.GetInMemoryConnection()
	ts.NoError(err)
	ts.users = repository.NewUserRepository(ts.db)
	ts.toker = token.NewTokenGenerator([]byte("test-key"))
	ts.enforcer = &mocks.Enforcer{}
	ts.service = NewAuthXService(ts.users, ts.toker, log.WithField("ctx", "test"), ts.enforcer)

	// migrate models
	ts.db.AutoMigrate(&model.User{})

	// create a test user
	ts.user = &model.User{
		Username: "tester",
		Password: "tester_password",
		Role:     "admin",
	}
	err = ts.users.Create(ts.user)
	ts.NoError(err)
	gin.SetMode(gin.ReleaseMode)

	if os.Getenv("DB_LOG_MODE") == "true" {
		ts.db.LogMode(true)
	}
}

func (ts *authxServiceTestSuite) TeardownTest() {
	core.CloseDB(ts.db)
}

func TestAuthXService(t *testing.T) {
	suite.Run(t, &authxServiceTestSuite{})
}

func (ts *authxServiceTestSuite) TestLoginHandler() {
	cases := map[string]map[string]string{
		"200": {
			"username": ts.user.Username,
			"password": ts.user.Password,
			"wanted":   "200",
		},
		"401": {
			"username": ts.user.Username,
			"password": ts.user.Password + "wrong_password",
			"wanted":   "401",
		},
		"400": {
			"username": "",
			"password": "",
			"wanted":   "400",
		},
	}
	for name, input := range cases {
		ts.Run("case "+name, func() {
			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			payload := map[string]string{
				"username": input["username"],
				"password": input["password"],
			}
			reqPayload, _ := json.Marshal(payload)
			req := httptest.NewRequest("POST", "/login", bytes.NewReader(reqPayload))
			c.Request = req

			ts.service.LoginHandler()(c)

			ts.Equal(input["wanted"], fmt.Sprintf("%d", w.Code))

			// assert responded token
			if input["wanted"] == "200" {
				rspPayload := map[string]string{}
				rspBody := make([]byte, w.Body.Len())
				_, err := w.Body.Read(rspBody)
				ts.NoError(err)
				err = json.Unmarshal(rspBody, &rspPayload)
				ts.NotEmpty(rspPayload["token"])
			}
		})
	}
}

func (ts *authxServiceTestSuite) TestValidateHandler() {
	tok, err := ts.toker.NewUserToken(ts.user)
	ts.NoError(err)
	cases := map[string]map[string]string{
		"200": {
			"token":          tok,
			"wanted_code":    "200",
			"wanted_user_id": "1",
			"enforcer": "true",
		},
		"400 empty": {
			"token":          "",
			"wanted_code":    "401",
			"wanted_user_id": "",
		},
		"400 invalid": {
			"token":          "invalid token",
			"wanted_code":    "401",
			"wanted_user_id": "",
		},
	}

	for name, input := range cases {
		ts.Run("case "+name, func() {
			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			payload := map[string]string{
				"token": input["token"],
			}
			reqPayload, _ := json.Marshal(payload)
			req := httptest.NewRequest("POST", "/validate", bytes.NewReader(reqPayload))
			c.Request = req

			// setup enforcer
			if v, ok := input["enforcer"]; ok && v == "true" {
				ts.enforcer.On("Enforce", mock.Anything, mock.Anything, mock.Anything).Return(true, nil)
			}

			ts.service.ValidateHandler()(c)

			ts.Equal(input["wanted_code"], fmt.Sprintf("%d", w.Code))

			// assert responded token
			if input["wanted_code"] == "200" {
				rspPayload := &pb.ValidateResponse{}
				rspBody := make([]byte, w.Body.Len())
				_, err := w.Body.Read(rspBody)
				ts.NoError(err)
				err = json.Unmarshal(rspBody, &rspPayload)
				ts.Equal(input["wanted_user_id"], fmt.Sprintf("%d", rspPayload.GetUserID()))
			}
		})
	}
}
