package service

import (
	"context"
	"jx/central/go/services/authx/pb"
	"net/http"
)

// Validate implements grpc handler
func (a *authxServiceImpl) Validate(ctx context.Context, req *pb.ValidateRequest) (*pb.ValidateResponse, error) {
	if err := a.validateInputRequest(req); err != nil {
		return nil, err
	}

	return a.authorizeRequest(req), nil
}

func (a *authxServiceImpl) validateInputRequest(req *pb.ValidateRequest) error {
	validating := ValidateRequest{
		Token: req.Token,
		Scope: req.Scope,
		Op:    req.Op,
	}

	err := a.getValidator().Struct(validating)
	return err
}

func (a *authxServiceImpl) authorizeRequest(req *pb.ValidateRequest) *pb.ValidateResponse {
	rsp := &pb.ValidateResponse{Code: http.StatusOK}

	// validate token
	claims, err := a.toker.ParseUserToken(req.Token)
	if err != nil {
		a.log.WithError(err).Debug("failed to parse token")
		rsp.Code = http.StatusUnauthorized
		rsp.Error = err.Error()
		return rsp
	} else {
		rsp.UserID = uint32(claims.ID)
		rsp.Role = claims.Role
	}

	// authorize requested endpoint
	result, err := a.enforcer.Enforce(rsp.Role, req.Scope, req.Op)
	if err != nil {
		rsp.Code = http.StatusForbidden
		rsp.Error = err.Error()
	} else if !result {
		rsp.Code = http.StatusForbidden
		rsp.Error = "Forbidden"
	}

	return rsp
}
