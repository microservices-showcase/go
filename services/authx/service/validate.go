package service

import (
	"github.com/gin-gonic/gin"
	"jx/central/go/services/authx/pb"
	"net/http"
)

// ValidateRequest ...
type ValidateRequest struct {
	Token string `json:"token" validate:"required"`
	Scope string `json:"scope" validate:"required"`
	Op    string `json:"op" validate:"required"`
}

// ValidateResponse ...
type ValidateResponse struct {
	Code   int    `json:"code"`
	UserID uint   `json:"user_id,omitempty"`
	Role   string `json:"role,omitempty"`
	Error  string `json:"error,omitempty"`
}

// ValidateHandler make a handler for http request
func (a *authxServiceImpl) ValidateHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		req := &ValidateRequest{}
		if err := c.ShouldBindJSON(req); err != nil {
			a.log.WithError(err).Error("bind data")
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		// use validate method
		reqData := &pb.ValidateRequest{
			Token: req.Token,
			Scope: req.Scope,
			Op:    req.Op,
		}
		rspData := a.authorizeRequest(reqData)

		c.JSON(int(rspData.Code), rspData)
	}
}
