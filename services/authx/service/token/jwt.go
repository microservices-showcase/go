package token

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"jx/central/go/services/authx/model"
	"time"
)

//go:generate mockery -name TokenGenerator

type UserClaims struct {
	ID   uint   `json:"id"`
	Role string `json:"role"`
	jwt.StandardClaims
}

// TokenGenerator ...
type TokenGenerator interface {
	New(claims jwt.Claims) (string, error)
	NewUserToken(user *model.User) (string, error)
	ParseUserToken(tokenS string) (*UserClaims, error)
}

type jwtGenerator struct {
	signingKey []byte
}

func (j *jwtGenerator) keyFunc(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
	}
	return j.signingKey, nil
}

func (j *jwtGenerator) ParseUserToken(tokenS string) (*UserClaims, error) {
	token, err := jwt.ParseWithClaims(tokenS, &UserClaims{}, j.keyFunc)
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(*UserClaims)
	if ok && token.Valid {
		return claims, nil
	}
	return nil, errors.New("unexpected claims")
}

// NewUserToken generate token for an user
func (j *jwtGenerator) NewUserToken(user *model.User) (string, error) {
	claims := &UserClaims{
		ID:   user.ID,
		Role: user.Role,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
			IssuedAt:  time.Now().Unix(),
			Issuer:    "authx",
		},
	}

	return j.New(claims)
}

func (j *jwtGenerator) New(claims jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, err := token.SignedString(j.signingKey)
	if err != nil {
		return "", err
	}
	return signedToken, nil
}

// NewTokenGenerator ...
func NewTokenGenerator(key []byte) TokenGenerator {
	return &jwtGenerator{signingKey: key}
}
