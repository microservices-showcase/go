package repository

import (
	"errors"
	"github.com/jinzhu/gorm"
	"jx/central/go/services/authx/model"
)

var ErrUserAlreadyExisted = errors.New("user already existed")

// UserRepository manipulate users
type UserRepository interface {
	Create(input *model.User) error
	FindByUsername(username string) (*model.User, error)
}

type userRepositoryImpl struct {
	db *gorm.DB
}

// Create make a new user
func (u *userRepositoryImpl) Create(input *model.User) error {
	existed, err := u.FindByUsername(input.Username)
	if err != nil {
		return err
	}
	if existed != nil {
		return ErrUserAlreadyExisted
	}
	// avoid to touch on password input
	creatingUser := &model.User{}
	*creatingUser = *input
	creatingUser.SetPassword([]byte(input.Password))
	err = u.db.Create(&creatingUser).Error
	if err != nil {
		return err
	}
	input.ID = creatingUser.ID
	return nil
}

// FindByUsername find a user by username
func (u *userRepositoryImpl) FindByUsername(username string) (*model.User, error) {
	result := &model.User{}
	err := u.db.Where("username = ?", username).First(&result).Error
	if gorm.IsRecordNotFoundError(err) {
		err = nil
		result = nil
	}
	return result, err
}

// NewUserRepository make new repository
func NewUserRepository(db *gorm.DB) UserRepository {
	return &userRepositoryImpl{db}
}

