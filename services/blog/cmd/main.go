package main

import (
	"github.com/caarlos0/env/v6"
	"github.com/gin-gonic/gin"
	"jx/central/go/lib/core"
	"jx/central/go/services/blog"

	"github.com/sirupsen/logrus"
	"jx/central/go/services/blog/model"
	"jx/central/go/services/blog/repository"
	"jx/central/go/services/blog/service"
)

var log *logrus.Entry

var (
	serviceName = "blog"
	version     = "0.0.0"
)

func init() {
	log = core.MakeLoggerByEnv("service:" + serviceName)
}

func main() {
	config := blog.AppConfig{}
	_ = env.Parse(&config)

	db, err := core.NewDBConnection(config.DBConfig)
	if err != nil {
		log.WithError(err).Error("failed to open DB connection")
		return
	}
	defer core.CloseDB(db)
	repo := repository.NewArticleRepository(db)
	svc := service.NewBlogService(repo, log)

	// register routes
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(core.GlobalTracingHandler(serviceName), core.GlobalRequestLog(serviceName,log))

	r.GET("/status", core.HealthHandler(serviceName, version))

	r.POST("/articles", svc.NewArticleHandler())
	r.GET("/articles/:article_id", svc.ViewArticleHandler())

	db.AutoMigrate(&model.Article{})

	v := make(chan struct{})
	go func() {
		if err := r.Run(config.ListenAddr); err != nil {
			log.WithError(err).Error("failed to start service")
			close(v)
		}
	}()
	log.WithField("listen_addr", config.ListenAddr).Info("start listening")
	<-v
}
