package service

import "jx/central/go/services/blog/model"

type articleCreateRequest struct {
	Title string `json:"title" binding:"required,min=3"`
	Body  string `json:"body" binding:"required,min=10"`
}

type articleGeneralResponse struct {
	ID       uint   `json:"id"`
	Title    string `json:"title"`
	Body     string `json:"body"`
	AuthorID uint   `json:"author_id"`
}

func newArticleGeneralResponse(article *model.Article) *articleGeneralResponse {
	return &articleGeneralResponse{
		ID:       article.ID,
		Title:    article.Title,
		Body:     article.Body,
		AuthorID: article.AuthorID,
	}
}
