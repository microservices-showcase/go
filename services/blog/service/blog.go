package service

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"jx/central/go/services/blog/model"
	"jx/central/go/services/blog/repository"
	"net/http"
	"strconv"
)

type BlogService interface {
	NewArticleHandler() gin.HandlerFunc
	ViewArticleHandler() gin.HandlerFunc
}

type blogServiceImpl struct {
	articles repository.ArticleRepository
	log      *logrus.Entry
}

func (b *blogServiceImpl) requestUserID(c *gin.Context) uint {
	sUserID := c.GetHeader("x-user-id")
	uid, err := strconv.Atoi(sUserID)
	if err != nil {
		return 0
	}
	return uint(uid)
}

func (b *blogServiceImpl) requestArticleID(c *gin.Context) uint {
	sArticleID := c.Param("article_id")
	aid, err := strconv.Atoi(sArticleID)
	if err != nil {
		return 0
	}
	return uint(aid)
}

func (b *blogServiceImpl) NewArticleHandler() gin.HandlerFunc {
	return b.newArticleGinHandler
}

func (b *blogServiceImpl) newArticleGinHandler(c *gin.Context) {
	userID := b.requestUserID(c)
	if userID == 0 {
		c.JSON(http.StatusForbidden, nil)
		return
	}
	req := &articleCreateRequest{}
	if err := c.ShouldBindJSON(req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	input := &model.Article{
		Title:    req.Title,
		Body:     req.Body,
		AuthorID: userID,
	}
	if err := b.articles.NewArticle(input); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusCreated, newArticleGeneralResponse(input))
}

func (b *blogServiceImpl) ViewArticleHandler() gin.HandlerFunc {
	return b.viewArticleGinHandler
}

func (b *blogServiceImpl) viewArticleGinHandler(c *gin.Context) {
	articleID := b.requestArticleID(c)
	if articleID == 0 {
		c.JSON(http.StatusNotFound, nil)
		return
	}
	article, err := b.articles.GetArticle(articleID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, newArticleGeneralResponse(article))
}

func NewBlogService(articles repository.ArticleRepository, log *logrus.Entry) BlogService {
	return &blogServiceImpl{
		articles: articles,
		log:      log,
	}
}
