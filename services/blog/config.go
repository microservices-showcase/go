package blog

import "jx/central/go/lib/core"

// AppConfig presents app config
type AppConfig struct {
	core.DBConfig

	ListenAddr string `env:"APP_LISTEN" envDefault:"0.0.0.0:8080"`
}

// test: trigger build this service. update 2020-06-02 1591096661
