package repository

import (
	"github.com/jinzhu/gorm"
	"jx/central/go/services/blog/model"
)

type ArticleRepository interface {
	NewArticle(input *model.Article) error
	GetArticle(id uint) (*model.Article, error)
}

type articleRepositoryImpl struct {
	db *gorm.DB
}

func (a *articleRepositoryImpl) NewArticle(input *model.Article) error {
	return a.db.Create(&input).Error
}

func (a *articleRepositoryImpl) GetArticle(id uint) (*model.Article, error) {
	result := &model.Article{}
	if err := a.db.First(&result, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			result = nil
		} else {
			return nil, err
		}
	}
	return result, nil
}

func NewArticleRepository(db *gorm.DB) ArticleRepository {
	return &articleRepositoryImpl{db}
}
