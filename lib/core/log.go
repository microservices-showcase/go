package core

import (
	"github.com/sirupsen/logrus"
	"os"
	"time"
)

// MakeLoggerByEnv make an logger with log level from environment
func MakeLoggerByEnv(tags string) *logrus.Entry {
	logger := logrus.New()
	logger.SetFormatter(&logrus.TextFormatter{TimestampFormat: time.RFC3339})
	logger.SetOutput(os.Stdout)
	envLevel := os.Getenv("LOG_LEVEL")
	level, err := logrus.ParseLevel(envLevel)
	if err != nil {
		level = logrus.InfoLevel
	}
	logger.SetLevel(level)

	log := logger.WithField("tags", tags)

	return log
}
