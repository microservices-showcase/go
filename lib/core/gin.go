package core

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"time"
)

// PanicOnError panic when error, we could catch errors on global middleware
func PanicOnError(err error) {
	if err != nil {
		panic(err)
	}
}

// GlobalTraceHandler ...
func GlobalTracingHandler(serviceName string) gin.HandlerFunc {
	return func(c *gin.Context) {
		current := c.GetHeader("x-trace")
		if current != "" {
			current += ";"
		}
		current += serviceName
		c.Header("x-trace", current)

		c.Next()
	}
}

// HealthHandler ...
func HealthHandler(serviceName, version string) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.JSON(200, gin.H{"status": "ok", "service": serviceName, "version": version})
	}
}

// NotFoundHandler
func NotFoundHandler() gin.HandlerFunc {
    return func(c *gin.Context) {
        c.JSON(404, gin.H{"status": "notfound", "url": c.Request.RequestURI})
    }
}

// GlobalRequestLog ...
func GlobalRequestLog(serviceName string, log *logrus.Entry) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery
		if raw != "" {
			path = path + "?" + raw
		}

		c.Next()

		end := time.Now()
		latency := end.Sub(start)
		log.
			WithField("status", c.Writer.Status()).
			WithField("method", c.Request.Method).
			WithField("path", path).
			WithField("ip", c.ClientIP()).
			WithField("latency", latency.String()).
			WithField("user-agent", c.Request.UserAgent()).
			WithField("service", serviceName).
			Info("Request")
	}
}

// trigger ci 2020-06-02 1591096685
