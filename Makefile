SHELL=/bin/bash

proto_files = ./services/*/pb/*.proto
package = jx/central/go

proto: clean gen-proto

install:
	@export GO111MODULE=on; \
		brew install protobuf; \
		go get google.golang.org/grpc@v1.28.1; \
		go get github.com/golang/protobuf/protoc-gen-go; \
		go get github.com/vektra/mockery/.../

gen-proto: $(proto_files)
	protoc -I . \
		-I ${GOPATH}/src \
		--go_out=plugins=grpc:. \
		--go_opt=paths=source_relative \
		$?

clean:
	rm -f ./services/*/pb/*.pb.go

